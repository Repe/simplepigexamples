package at.fhv.pig;

import org.apache.pig.PrimitiveEvalFunc;

import java.io.IOException;

public class Trim extends PrimitiveEvalFunc<String, String> {

    @Override
    public String exec(String s) throws IOException {
        // removes leading and trailing whitespaces

        return s.trim();
    }
}
