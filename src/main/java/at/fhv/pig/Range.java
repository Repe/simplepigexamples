package at.fhv.pig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class Range {

    private final int start;
    private final int end;

    private Range(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int start() {
        return start;
    }

    public int end() {
        return end;
    }

    public String substring(String line) {
        return line.substring(start - 1, end);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Range range = (Range) o;
        return start == range.start &&
                end == range.end;
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }


    public static List<Range> parse(String rangeSpec)
            throws IllegalArgumentException {
        if (rangeSpec.length() == 0) {
            return Collections.emptyList();
        }
        List<Range> ranges = new ArrayList<Range>();
        String[] specs = rangeSpec.split(",");
        for (String spec : specs) {
            String[] split = spec.split("-");
            try {
                ranges.add(new Range(Integer.parseInt(split[0]), Integer
                        .parseInt(split[1])));
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return ranges;
    }


}
