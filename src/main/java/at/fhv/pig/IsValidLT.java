package at.fhv.pig;


import org.apache.pig.FilterFunc;
import org.apache.pig.FuncSpec;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IsValidLT extends FilterFunc {

    @Override
    public Boolean exec(Tuple tuple) throws IOException {
        if (tuple == null || tuple.size() < 2) {
            return false;
        }

        try {
            return isValidTemperature(tuple.get(0)) && isValidQuality(tuple.get(1));

        } catch (ExecException e) {
            throw new IOException(e);
        }
    }

    @Override
    public List<FuncSpec> getArgToFuncMapping() throws FrontendException {

        List<Schema.FieldSchema> fields = new ArrayList<>();
        fields.add(new Schema.FieldSchema(null, DataType.INTEGER));
        fields.add(new Schema.FieldSchema( null, DataType.CHARARRAY));


        List<FuncSpec> funcSpecs = new ArrayList<>();
        funcSpecs.add(new FuncSpec(this.getClass().getName(),
                new Schema(fields
                        )));

        return funcSpecs;
    }


    private static boolean isValidTemperature(Object object) {
        if (object == null) {
            return false;
        }

        int temp = (Integer) object;

        return temp != 9999;
    }

    private static boolean isValidQuality(Object object) {
        if (object == null) {
            return false;
        }

        String quality = (String)object;

        return quality.matches("[01459]");
    }



}
