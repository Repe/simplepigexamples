package at.fhv.pig;

import org.apache.pig.FilterFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.Tuple;

import java.io.IOException;

public class IsValid extends FilterFunc {

    @Override
    public Boolean exec(Tuple tuple) throws IOException {
        if (tuple == null || tuple.size() < 2) {
            return false;
        }

        try {
            return isValidTemperature(tuple.get(0)) && isValidQuality(tuple.get(1));

        } catch (ExecException e) {
            throw new IOException(e);
        }
    }


    private static boolean isValidTemperature(Object object) {
        if (object == null) {
            return false;
        }

        int temp = (Integer) object;

        return temp != 9999;
    }

    private static boolean isValidQuality(Object object) {
        if (object == null) {
            return false;
        }

        String quality = (String)object;

        return quality.matches("[01459]");
    }
}
